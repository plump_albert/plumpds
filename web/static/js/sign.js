/** @type {HTMLInputElement} */
const filePreview = {
	source: document.querySelector("div#sourcePreview"),
	publicKey: document.querySelector("div#publicKeyPreview"),
	signature: document.querySelector("div#signaturePreview"),
	privateKey: document.querySelector("div#privateKeyPreview"),
};
let files = {};

function onFileChange(e) {
	if (!e.target.files[0] && !files[e.target.name]) {
		filePreview[e.target.name].children[0].classList.add("hidden");
		filePreview[e.target.name].children[1].innerText = "File not selected";
		filePreview[e.target.name].children[2].classList.add("hidden");
		return;
	}
	files[e.target.name] = e.target.files[0];
	filePreview[e.target.name].children[0].classList.remove("hidden");
	filePreview[e.target.name].children[2].classList.remove("hidden");
	filePreview[e.target.name].children[1].innerText = files[e.target.name].name;
}
document.querySelector("input#source").addEventListener("change", onFileChange);
const publicKey = document.querySelector("input#publicKey");
if (publicKey) publicKey.addEventListener("change", onFileChange);
const privateKey = document.querySelector("input#privateKey");
if (privateKey) privateKey.addEventListener("change", onFileChange);
const signature = document.querySelector("input#signature");
if (signature) signature.addEventListener("change", onFileChange);
