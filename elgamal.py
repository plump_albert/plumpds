import base64
import math
import random
from rsa.rsa import prime
import hashlib
import os.path as path


def gcd(a: int, b: int) -> int:
    if b == 0:
        return a
    else:
        return gcd(b, a % b)


def prim_root(modulo):
    s = set()
    phi = modulo - 1
    while (phi % 2 == 0):
        s.add(2)
        phi = phi // 2
    for i in range(3, int(math.sqrt(phi)), 2):
        while (phi % i == 0):
            s.add(i)
            phi = phi // i
    if (phi > 2):
        s.add(phi)
    for r in range(2, modulo):
        flag = False
        for it in s:
            if (pow(r, (modulo - 1) // it, modulo) == 1):
                flag = True
                break
        if (flag == False):
            return r
    return None


def read_file(file: bytes) -> list[int]:
    result = []

    def read_data(index: int) -> tuple[bytes, int]:
        size = int.from_bytes(
            file[index: index + 4],
            byteorder='big', signed=False
        )
        index += 4
        return (file[index:index + size], index + size)

    index = 0
    while index < len(file):
        number, index = read_data(index)
        result.append(int.from_bytes(number, byteorder='big'))
    return result


def sign(message: bytes, private_key: bytes):
    p, g, x = read_file(base64.b64decode(private_key))
    # Generate session key
    while True:
        k = random.randrange(2, p)
        if gcd(k, p - 1) == 1:
            break
    message_hash = int.from_bytes(
        hashlib.sha512(message).digest(),
        byteorder='big'
    )
    r = pow(g, k, p)
    _, _, inv_k = prime.extended_gcd(p - 1, k)
    s = (inv_k * (message_hash - x * r)) % (p - 1)
    signature = bytearray()
    for i in (r, s):
        i_size = (i.bit_length() + 7) // 8
        signature += i_size.to_bytes(4, byteorder='big')
        signature += i.to_bytes(i_size, byteorder='big')
    return signature


def check(message: bytes, signature: bytes, public_key: bytes):
    p, g, y = read_file(base64.b64decode(public_key))
    r, s = read_file(signature)
    message_hash = int.from_bytes(
        hashlib.sha512(message).digest(),
        byteorder='big'
    )
    return (pow(y, r, p) * pow(r, s, p)) % p == pow(g, message_hash, p)


def init_keys(complexity: int = 128):
    p = prime.get_prime(complexity)
    g = prim_root(p)
    if not g:
        raise Exception("Cannot find primitive root for `%d`" % p)
    # Generate x
    x = random.randrange(2, p)
    # Calculate y
    y = pow(g, x, p)
    public_key = bytearray()
    for i in (p, g, y):
        i_size = (i.bit_length() + 7) // 8
        public_key += i_size.to_bytes(4, byteorder='big')
        public_key += i.to_bytes(i_size, byteorder='big')
    private_key = bytearray()
    for i in (p, g, x):
        i_size = (i.bit_length() + 7) // 8
        private_key += i_size.to_bytes(4, byteorder='big')
        private_key += i.to_bytes(i_size, byteorder='big')
    return (public_key, private_key)
