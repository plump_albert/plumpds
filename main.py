from io import BytesIO
from base64 import b64encode
from zipfile import ZIP_BZIP2, ZipFile
from flask import Flask, Response, request, send_file, render_template
from rsa.rsa import keymaker as rsa_keymaker
from rsa.rsa import cipher as rsa_cipher
import elgamal

app = Flask(__name__,
            static_url_path='',
            static_folder='web/static',
            template_folder='web/templates')


@app.route("/")
def index():
    return send_file('web/static/index.html')


@app.route("/generate/<method>")
def generate(method):
    context = {}
    context['method'] = method.upper()
    context['method_url'] = method
    return render_template('generate.html.j2', context=context)


@app.route('/generate/<method>', methods=['POST'])
def generate_rsa(method):
    if method == 'rsa':
        public, private = rsa_keymaker.generate_keys(
            complexity=request.args.get('complexity') or 256
        )
    elif method == 'elgamal':
        public, private = elgamal.init_keys(
            request.args.get('complexity') or 32)
    else:
        return Response('Method not allowed', 405)
    stream = BytesIO()
    with ZipFile(stream, 'w') as zf:
        zf.writestr('key.pub', (method + ' ').encode() + b64encode(public))
        zf.writestr('key',  (method + ' ').encode() + b64encode(private))
    stream.seek(0)
    return send_file(
        stream,
        as_attachment=True,
        attachment_filename=f'{method}_keys.zip'
    )


@app.route("/signing/sign/<method>")
def sign(method):
    context = {}
    context['method'] = method.upper()
    context['method_url'] = method
    return render_template('sign.html.j2', context=context)


@app.route("/signing/sign/<method>", methods=['POST'])
def signFile(method):
    source = BytesIO()
    request.files.get('source').save(source)
    source.seek(0)
    source = source.read()
    privateKey = BytesIO()
    request.files.get('privateKey').save(privateKey)
    privateKey.seek(0)
    privateKey = privateKey.read()
    if method == 'rsa':
        signature_file = BytesIO(rsa_cipher.sign(source, privateKey[4:]))
    elif method == 'elgamal':
        signature_file = BytesIO(elgamal.sign(source, privateKey[8:]))
    else:
        return Response('Method not allowed', 405)
    return send_file(
        signature_file,
        as_attachment=True,
        attachment_filename=request.files.get(
            'source').filename + '.' + method + '.sig'
    )


@app.route("/signing/check/<method>")
def check(method):
    context = {}
    context['method'] = method.upper()
    context['method_url'] = method
    return render_template('check.html.j2', context=context)


@app.route("/signing/check/<method>", methods=['POST'])
def checkFile(method):
    source = BytesIO()
    request.files.get('source').save(source)
    source.seek(0)
    source = source.read()

    signature = BytesIO()
    request.files.get('signature').save(signature)
    signature.seek(0)
    signature = signature.read()

    publicKey = BytesIO()
    request.files.get('publicKey').save(publicKey)
    publicKey.seek(0)
    publicKey = publicKey.read()
    if method == 'rsa':
        check_result = rsa_cipher.check_sign(source, signature, publicKey[4:])
    elif method == 'elgamal':
        check_result = elgamal.check(source, signature, publicKey[8:])
    else:
        return Response('Method not allowed', 405)
    if check_result:
        return send_file('./web/static/success.html')
    else:
        return send_file('./web/static/error.html')
