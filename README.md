# Plump Digital Signature

Project for creating digital signatures for different files using RSA and
Elgamal algorithms

## TODO

- [x] Implement Elgamal algorithm;
- [ ] Create frontend UI for signing files using RSA and Elgamal algorithms.
