FROM python:3.10-alpine
COPY . /var/www/
WORKDIR /var/www
RUN pip3 install -r requirements.txt
ENV FLASK_APP=main.py
ENV FLASK_ENV=production
CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]
